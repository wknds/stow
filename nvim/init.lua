local o = vim.o

o.number = true
o.syntax = true
o.clipboard = 'unnamedplus'
