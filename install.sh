#!/bin/bash
apt install \
	git \
	tmux \
	xclip \ # command line interface for X selections (für nvim)

mkdir -p ~/.config/nvim/plugged

stow -t ~ git
stow -t ~/.config/nvim nvim
